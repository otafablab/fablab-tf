# Fablab terraform

## Commands

### Destroy a set of resources using globbing

```fish
for target in module.{a2,a3,a4,b1,b2}.{libvirt_domain.domain,libvirt_volume.disk_resized}; echo "-target $target"; end |xargs -r --open-tty terraform destroy
```

### Run ad hoc commands on a set of hosts using globbing

```fish
echo root@10.42.0.(seq 13 16) |pdsh -w- virsh pool-list
```

Output:

```text
root@10.42.0.16:  Name      State    Autostart
root@10.42.0.16: -------------------------------
root@10.42.0.16:  default   active   yes
root@10.42.0.16:
root@10.42.0.14:  Name      State    Autostart
root@10.42.0.14: -------------------------------
root@10.42.0.14:  default   active   yes
root@10.42.0.14:
root@10.42.0.13:  Name      State    Autostart
root@10.42.0.13: -------------------------------
root@10.42.0.13:  default   active   yes
root@10.42.0.13:
root@10.42.0.15:  Name      State    Autostart
root@10.42.0.15: -------------------------------
root@10.42.0.15:  default   active   yes
root@10.42.0.15:
```

## Resources

- https://spacelift.io/blog/terraform-commands-cheat-sheet
