resource "libvirt_pool" "default" {
  name	 		= "default"
  type			= "dir"
  path			= "/var/lib/libvirt/pools/default"
}

resource "libvirt_network" "host-bridge" {
  name			= "host-bridge"
  mode			= "bridge"
  bridge		= "br0" # created by nixos configuration in fablab-nix
  autostart		= true
}

resource "libvirt_volume" "base_os" {
  name			= "base_os.qcow2"
  pool			= libvirt_pool.default.name
  source		= var.base_os_source
  format		= "qcow2"
}

# TODO check https://developer.hashicorp.com/terraform/language/meta-arguments/lifecycle for more options
resource "libvirt_volume" "disk_resized" {
  count                 = var.replicas
  name			= "${var.domain_name}-${count.index}.qcow2"
  base_volume_id	= libvirt_volume.base_os.id
  pool			= libvirt_pool.default.name
  size			= 1024 * 1024 * 1024 * var.system_volume
  format		= "qcow2"
}

resource "libvirt_cloudinit_disk" "cloudinit" {
  count                 = var.replicas
  name			= "cloudinit-${count.index}.iso"
  pool			= libvirt_pool.default.name
  user_data		= templatefile(
    "${path.module}/templates/cloud_init.yml.tpl",
    {
      hostname          = "${var.domain_name}-${count.index}"
      ssh_keys          = var.ssh_keys
      runcmd            = var.runcmd
    }
  )
  network_config	= templatefile(
    "${path.module}/templates/network_config.yml.tpl",
    {
      nic		= var.nic
    }
  )
}

resource "libvirt_domain" "domain" {
  count			= var.replicas
  name			= "${var.domain_name}-${count.index}"
  memory		= var.memory
  vcpu			= var.vcpu
  autostart		= var.autostart
  qemu_agent		= true

  cloudinit		= element(libvirt_cloudinit_disk.cloudinit[*].id, count.index)

  cpu {
    mode                = "host-passthrough"
  }
  
  network_interface {
    network_id          = libvirt_network.host-bridge.id
    wait_for_lease	= true
  }

  xml {
    xslt		= templatefile("${path.module}/xslt/template.tftpl", var.xml_override)
  }

  console {
    type		= "pty"
    target_port		= "0"
    target_type		= "serial"
  }

  console {
    type		= "pty"
    target_port		= "1"
    target_type		= "virtio"
  }

  disk {
    volume_id		= element(libvirt_volume.disk_resized[*].id, count.index)
  }

  # NFS share will be used later (NOTE the nic will be ens4 if enabled)
  # filesystem {
  #   accessmode        = "passthrough"
  #   source            = "/nas/home"
  #   target            = "nas"
  #   readonly          = false
  # }
  
  graphics {
    type		= "spice"
    listen_type		= "address"
    autoport		= true
  }

  provisioner "remote-exec" {
    inline		= [
      "echo \"Virtual Machine \"$(hostname)\" is UP!\"",
      "date",
      "sudo journalctl --no-pager -u cloud-init"
    ]
    connection {
      type		= "ssh"
      user		= "localadmin"
      host		= self.network_interface[0].addresses[0]
      timeout		= "30s"
    }
  }

  lifecycle {
    ignore_changes	= [
      network_interface, # seems to be a bug in libvirt provider
    ]
  }
}

output "ip" {
  value = {
    for domain in libvirt_domain.domain:
      domain.name => domain.network_interface[0].addresses[0]
  }
}
