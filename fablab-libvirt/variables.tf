variable "domain_name" {
  description = "Base name of the virtual machine. Will get an incremental suffix"
  type = string
}

variable "base_os_source" {
  description = "Source URL of the base server image"
  type = string
}

variable "autostart" {
  description = "Autostart the domain"
  type        = bool
  default     = true
}

variable "memory" {
  description = "RAM in MB"
  type        = string
  default     = "4096"
}

variable "replicas" {
  description = "Number of replicas to provision"
  type        = number
  default     = 1
}

variable "xml_override" {
  description = "With these variables you can: Enable hugepages; Set USB controllers; Attach USB devices"
  type = object({
    usb_controllers = list(object({
      model = string
    }))
    usb_devices = list(object({
      vendor  = string
      product = string
    }))
  })
  default = {
    usb_controllers = [
      {
        model = "piix3-uhci"
      }
    ],
    usb_devices = []
  }
}

variable "vcpu" {
  description = "Number of vCPUs"
  type        = number
  default     = 1
}

variable "system_volume" {
  description = "System Volume size (GiB)"
  type        = number
  default     = 20
}

variable "runcmd" {
  description = "Extra commands to be run with cloud init"
  type        = list(string)
  default     = []
}

variable "nic" {
  description = "Name of the primary NIC in the server"
  type        = string
  default     = "ens3"  # TODO why does this keep chanigng
}

variable "ssh_keys" {
  description = "List of public SSH keys which can log in"
  type        = list(string)
  default     = []
}

variable "hetznerdns_zone_id" {
  description = "ID of the hetzner DNS zone to create records to"
  type = string
}
