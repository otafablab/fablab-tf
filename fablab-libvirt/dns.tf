resource "hetznerdns_record" "internal_fablab_rip" {
  count   = var.replicas
  zone_id = var.hetznerdns_zone_id
  name    = format("%s.internal", element(libvirt_domain.domain[*].name, count.index))
  value   = element(libvirt_domain.domain[*].network_interface[0].addresses[0], count.index)
  type    = "A"
}
