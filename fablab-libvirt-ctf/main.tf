resource "libvirt_pool" "default" {
  name	 		= "ctf"
  type			= "dir"
  path			= "/var/lib/libvirt/pools/ctf"
}

resource "libvirt_network" "host-bridge" {
  name			= "host-bridge"
  mode			= "bridge"
  bridge		= "br0" # created by nixos configuration in fablab-nix
  autostart		= true
}

resource "libvirt_network" "ctf" {
  name			= "ctf"
  mode			= "nat"
  addresses             = ["10.17.3.0/24"]
  autostart		= true
}

resource "libvirt_volume" "ubuntu_os" {
  name			= "ubuntu_os.qcow2"
  pool			= libvirt_pool.default.name
  source		= "ubuntu"
  format		= "qcow2"
}

resource "libvirt_volume" "debian_os" {
  name			= "debian_os.qcow2"
  pool			= libvirt_pool.default.name
  source		= "debian"
  format		= "qcow2"
}

resource "libvirt_volume" "fedora_os" {
  name			= "fedora_os.qcow2"
  pool			= libvirt_pool.default.name
  source		= "fedora"
  format		= "qcow2"
}

resource "libvirt_volume" "nix_os" {
  name			= "nix_os.qcow2"
  pool			= libvirt_pool.default.name
  source		= "nixos"
  format		= "qcow2"
}

# TODO check https://developer.hashicorp.com/terraform/language/meta-arguments/lifecycle for more options
resource "libvirt_volume" "ubuntu_resized" {
  count                 = var.ubuntu_replicas
  name			= "ctf-ubuntu-${count.index}.qcow2"
  base_volume_id	= libvirt_volume.ubuntu_os.id
  pool			= libvirt_pool.default.name
  size			= 1024 * 1024 * 1024 * 10
  format		= "qcow2"
}

resource "libvirt_volume" "debian_resized" {
  count                 = var.debian_replicas
  name			= "ctf-debian-${count.index}.qcow2"
  base_volume_id	= libvirt_volume.debian_os.id
  pool			= libvirt_pool.default.name
  size			= 1024 * 1024 * 1024 * 10
  format		= "qcow2"
}

resource "libvirt_volume" "fedora_resized" {
  count                 = var.fedora_replicas
  name			= "ctf-fedora-${count.index}.qcow2"
  base_volume_id	= libvirt_volume.fedora_os.id
  pool			= libvirt_pool.default.name
  size			= 1024 * 1024 * 1024 * 10
  format		= "qcow2"
}

resource "libvirt_volume" "nix_resized" {
  count                 = var.nix_replicas
  name			= "ctf-nix-${count.index}.qcow2"
  base_volume_id	= libvirt_volume.nix_os.id
  pool			= libvirt_pool.default.name
  size			= 1024 * 1024 * 1024 * 10
  format		= "qcow2"
}



resource "libvirt_cloudinit_disk" "ubuntu_cloudinit" {
  count                 = var.ubuntu_replicas
  name			= "cloudinit-${count.index}.iso"
  pool			= libvirt_pool.default.name
  user_data		= templatefile(
    "${path.module}/templates/cloud_init.yml.tpl",
    {
      hostname          = "${var.domain_name}-${count.index}"
      ssh_keys          = var.ssh_keys
      runcmd            = var.runcmd
    }
  )
  network_config	= templatefile(
    "${path.module}/templates/network_config.yml.tpl",
    {
      nic		= var.nic
    }
  )
}

resource "libvirt_domain" "ubuntu" {
  count			= var.ubuntu_replicas
  name			= "ctf-ubuntu-${count.index}"
  memory		= var.memory
  vcpu			= var.vcpu
  autostart		= var.autostart
  qemu_agent		= true

  cloudinit		= element(libvirt_cloudinit_disk.ubuntu_cloudinit[*].id, count.index)

  cpu {
    mode                = "host-passthrough"
  }
  
  network_interface {
    network_id          = libvirt_network.host-bridge.id
    wait_for_lease	= true
  }

  xml {
    xslt		= templatefile("${path.module}/xslt/template.tftpl", var.xml_override)
  }

  console {
    type		= "pty"
    target_port		= "0"
    target_type		= "serial"
  }

  console {
    type		= "pty"
    target_port		= "1"
    target_type		= "virtio"
  }

  disk {
    volume_id		= element(libvirt_volume.disk_resized[*].id, count.index)
  }

  graphics {
    type		= "spice"
    listen_type		= "address"
    autoport		= true
  }

  provisioner "remote-exec" {
    inline		= [
      "echo \"Virtual Machine \"$(hostname)\" is UP!\"",
      "date",
      "sudo journalctl --no-pager -u cloud-init"
    ]
    connection {
      type		= "ssh"
      user		= "localadmin"
      host		= self.network_interface[0].addresses[0]
      timeout		= "30s"
    }
  }

  lifecycle {
    ignore_changes	= [
      network_interface, # seems to be a bug in libvirt provider
    ]
  }
}

output "ip" {
  value = {
    for domain in libvirt_domain.ubuntu_domain:
      domain.name => domain.network_interface[0].addresses[0]
  }
}
