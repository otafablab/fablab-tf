#cloud-config
package_upgrade: false

packages:
  - iotop
  - python3
  - qemu-guest-agent

# Documentation
# https://cloudinit.readthedocs.io/en/latest/reference/examples.html#run-commands-on-first-boot

# bootcmd will run on every boot
bootcmd:
  - date +"%T.%3N" > /tmp/bootcmd

# runcmd only runs during the first boot
runcmd:
  - date +"%T.%3N" > /tmp/runcmd
  - systemctl start qemu-guest-agent
  - systemctl restart systemd-networkd
  - mkdir -p /nas/home
  - mount -a
%{for cmd in runcmd~}
  - ${cmd}
%{endfor~}

fqdn: ${hostname}

ssh_pwauth: false

users:
  - name: localadmin # admin is already a reserved group
    passwd: "$y$j9T$iu.POlAreL4qVGo9lOZp..$0B0BomFqm7LncpmRFt5jmYRYzWElxJhSFymyG3PBgzC"
    uid: 1000
    lock_passwd: false
    groups: sudo
    sudo: ALL=(ALL) NOPASSWD:ALL
    shell: /bin/bash
    ssh_authorized_keys:
%{for key in ssh_keys~}
      - ${key}
%{endfor~}
#  - name: user1
#    uid: 1001
#    lock_passwd: false
#    groups: sudo
#    sudo: ALL=(ALL) NOPASSWD:ALL
#    shell: /bin/bash
#    homedir: /nfs/home/user1

growpart:
    mode: auto
    devices:
      - "/"

resize_rootfs: true

# Will be used later
#mounts:
# - [ "nas", "/nas/home", "virtiofs", "rw,noatime", "0", "0" ]