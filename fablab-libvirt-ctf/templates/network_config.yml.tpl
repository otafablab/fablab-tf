version: 2
ethernets:
  ${nic}:
    dhcp4: true
# bridge configuration might be useful in the future
# bridges:
#   br0:
#     dhcp4: true
#     interfaces:
#        - ${nic}