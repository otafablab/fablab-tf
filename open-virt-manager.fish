#!/usr/bin/env fish

for host in $argv
    virt-manager -c "qemu+ssh://$host/system"
end
