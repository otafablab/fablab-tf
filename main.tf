terraform {
  required_version = ">= 1.0"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.7.1"
    }
    hetznerdns = {
      source  = "timohirt/hetznerdns"
      version = "2.2.0"
    }
  }
}

variable "hetznerdns_token" {}

provider "hetznerdns" {
  apitoken = var.hetznerdns_token
}

resource "hetznerdns_zone" "fablab_rip" {
  name = "fablab.rip"
  ttl = 86400
  lifecycle {
    prevent_destroy = true # The zone contains records outside the scope
  }
}

# Unfortunately no "count" support for providers (ever) https://stackoverflow.com/questions/63915229/terraform-0-13-modules-for-each-and-providers
provider "libvirt" {
  alias = "a2"
  uri = "qemu+ssh://admin@10.42.0.2/system"
}

module "a2" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.a2
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "a2-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "a3"
  uri = "qemu+ssh://admin@10.42.0.3/system"
}

module "a3" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.a3
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "a3-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "a4"
  uri = "qemu+ssh://admin@10.42.0.4/system"
}

module "a4" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.a4
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "a4-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "b1"
  uri = "qemu+ssh://admin@10.42.0.5/system"
}

module "b1" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.b1
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "b1-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "b2"
  uri = "qemu+ssh://admin@10.42.0.6/system"
}

module "b2" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.b2
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "b2-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "b3"
  uri = "qemu+ssh://admin@10.42.0.7/system"
}

module "b3" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.b3
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "b3-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "b4"
  uri = "qemu+ssh://admin@10.42.0.8/system"
}

module "b4" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.b4
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "b4-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "c1"
  uri = "qemu+ssh://admin@10.42.0.9/system"
}

module "c1" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.c1
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "c1-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "c2"
  uri = "qemu+ssh://admin@10.42.0.10/system"
}

module "c2" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.c2
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "c2-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "c3"
  uri = "qemu+ssh://admin@10.42.0.11/system"
}

module "c3" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.c3
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "c3-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "c4"
  uri = "qemu+ssh://admin@10.42.0.12/system"
}

module "c4" {
  source    = "./fablab-libvirt-ctf"
  providers = {
    libvirt = libvirt.c4
  }

  ssh_keys = local.ssh_keys
}

provider "libvirt" {
  alias = "d1"
  uri = "qemu+ssh://admin@10.42.0.13/system"
}

module "d1" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.d1
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "d1-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "d2"
  uri = "qemu+ssh://admin@10.42.0.14/system"
}

module "d2" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.d2
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "d2-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "d3"
  uri = "qemu+ssh://admin@10.42.0.15/system"
}

module "d3" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.d3
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "d3-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

provider "libvirt" {
  alias = "d4"
  uri = "qemu+ssh://admin@10.42.0.16/system"
}

module "d4" {
  source    = "./fablab-libvirt"
  providers = {
    libvirt = libvirt.d4
  }

  replicas = 3
  ssh_keys = local.ssh_keys
  domain_name = "d4-ubuntu"
  base_os_source = local.ubuntu_22_img_local
  hetznerdns_zone_id = hetznerdns_zone.fablab_rip.id
}

output "ip" {
  value = {
    a2 = module.a2.ip
    a3 = module.a3.ip
    a4 = module.a4.ip
    b1 = module.b1.ip
    b2 = module.b2.ip
    b3 = module.b3.ip
    b4 = module.b4.ip
    c1 = module.c1.ip
    c2 = module.c2.ip
    c3 = module.c3.ip
    c4 = module.c4.ip
    d1 = module.d1.ip
    d2 = module.d2.ip
    d3 = module.d3.ip
    d4 = module.d4.ip
  }
}
